import React, { Component } from 'react';
import {render} from 'react-dom';

import {TreeContainer, TreeStore} from 'treeOfLife/';
import {QueryBoxContainer, QueryBoxStore} from 'queryBox';
import {SummaryBoxContainer, SummaryBoxStore} from 'summaryBox';

import AppDispatcher from './dispatcher/AppDispatcher';
import TreeActionHandler from './actionHandler/TreeActionHandler';
import QueryBoxActionHandler from './actionHandler/QueryBoxActionHandler'; 

import * as AppConstant from './AppConstant';
import * as QueryBoxConstant from 'queryBox/QueryBoxConstant';
import * as Config from './Config';
import DonutChart from 'DonutChart/';

/*Services*/
import QueryService from 'QueryService';
import TreeService from 'TreeService';

import 'style/';
import * as Util from 'util/';
/*----------  store initialization  ----------*/

// tree store initialization

function initializeServices(){

  var queryService = new QueryService({
    baseUrl: Config.SERVER_BASE_URL,
  });
  var treeService = new TreeService({
    baseUrl: Config.SERVER_BASE_URL,
  });
  var services = {queryService, treeService};
  return Promise.resolve(services);
}

function initializeStores(services){
  const innerRadius = 370;
  const outerRadius = 500;
  const viewDepth = 10;
  // QueryBox store initialization
  var queryBoxStore = new QueryBoxStore();
  var qtype = Util.getQueryParamByName('qtype');
  var qstring = Util.getQueryParamByName('qstring');
  function qtypeIndex(qtype){
    return QueryBoxConstant.QUERY_BOX_OPTIONS.map((x)=>x.displayText).indexOf(qtype);
  }
  if (qtype && qstring && qtypeIndex(qtype) >= 0){
    queryBoxStore.selectedOption = QueryBoxConstant.QUERY_BOX_OPTIONS[qtypeIndex(qtype)];
    queryBoxStore.query = qstring;
  }
  var summaryBoxStore = new SummaryBoxStore();
  // return a promise
  return Promise.all([services.treeService.getPrunedTree(),
    services.treeService.getUnprunedTree()])
    .then(function(trees){
      var prunedTree = trees[0];
      var unprunedTree = trees[1];
      var treeStore = new TreeStore({prunedTree,unprunedTree,innerRadius,outerRadius,viewDepth});
      // TODO, set the other tree as well
      return {queryBoxStore,summaryBoxStore, treeStore};
    });
}

/*----------  handler initialization  ----------*/

function initializeHandlers(stores,services, AppDispatcher){
  var handlers = [TreeActionHandler,QueryBoxActionHandler].map((HandlerClass) => new HandlerClass({stores,services,AppDispatcher}));
  return Promise.resolve(handlers);
}

// link stores and action handlers

class App extends Component {
  static proptypes = {
    stores: React.PropTypes.object.isRequired,
    services: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
  }
  constructor(props) {
    super(props);
  }
  render(){
    var {treeStore, summaryBoxStore, queryBoxStore} = this.props.stores;
    return (
      <div onClick={(e)=>AppDispatcher.handleViewAction({
        type: AppConstant.APP_CLICKED,
        payload: e
      })}>
        <header className="appHeader">
          <div className="row ">
            <div className="col-sm-2">
              <div className="brand">
                PhyDo
              </div>
            </div>
            <div className="col-sm-7">
              <QueryBoxContainer queryBoxStore={queryBoxStore}></QueryBoxContainer>
            </div>
            <div className="col-sm-3">
              <ul className="actions">
                <li className="action-item">
                  <a href="/workflow.html" target="_blank">workflow</a>
                </li>
                <li className="action-item">
                  <a href="/known_issues.html" target="_blank">issues</a>
                </li>
              </ul>
            </div>
          </div>
        </header>
        <div className="row">
          <div className="col-sm-8">
            <TreeContainer treeStore={treeStore}></TreeContainer>
          </div>
          <div className="col-sm-4">
            <SummaryBoxContainer summaryBoxStore={summaryBoxStore}></SummaryBoxContainer>
          </div>
        </div>
      </div>
    );
    // note that {...{treeStore}} is same as using prop treeStore=treeStore
  }
};


/*
  TODO 
  Using dependency injection, delay the rendering of App until all services are resolved
  for TreeStore, perhaps we can use a null value first, then wait till the tree is fully resolved (later)

*/

function bootstrapApp(){
  initializeServices()
    .then(function(services){ 
      return initializeStores(services).then(function(stores){
        return {services,stores};
      });
    })
    .then(function(servicesAndStores){
      var services = servicesAndStores.services;
      var stores = servicesAndStores.stores;
      initializeHandlers(stores,services,AppDispatcher);
      render(<App stores={stores}/>, document.getElementById('root'));
    });
}

bootstrapApp();


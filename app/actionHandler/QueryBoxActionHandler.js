import ViewActionHandler from './ViewActionHandler';
import * as QueryBoxConstant from 'queryBox/QueryBoxConstant';
import * as AppConstant from 'AppConstant';
import * as _ from 'lodash';

export default class QueryBoxActionHandler extends ViewActionHandler{
  constructor(props){
    super(props);
    this.queryBoxStore = this.stores.queryBoxStore;
    this.treeStore = this.stores.treeStore;
    this.summaryBoxStore = this.stores.summaryBoxStore;
    this.queryService = this.services.queryService;
  }

  checkQueryTextValid(queryText){
    // except for the last phrase, every phrase before should 
    // be pfam accession number
    if(this.queryBoxStore.selectedOption === QueryBoxConstant.Architecture){
      if(!/^(((PF\d{5})|\*), )*(\w*|\*)$/.test(queryText)){
        return {
          message: 'Query is invalid, it must be comma separated pfam id. E.g. PF00001, PF00002',
        };
      }
    }
    else if (this.queryBoxStore.selectedOption === QueryBoxConstant.Domain)
      // don't allow * in domain
      if(!/^(((PF\d{5})), )*(\w*)$/.test(queryText)){
        return {
          message: 'Query is invalid, it must be comma separated pfam id. E.g. PF00001, PF00002. \n * is not allowed in this search',
        };
      } 
    return;
  }
  // return an array of query phrases from the text
  getQueryPhrases(queryText){
    return queryText.split(',').map((s)=>s.trim()).filter((t)=>t.length>0);
  }
  // return the phrase that will be used to query server for suggestions
  getAutocompletePhrase(queryText){
    if (queryText === ''){
      return '';
    }
    this.checkQueryTextValid(queryText);
    var phrases = this.getQueryPhrases(queryText);
    if (phrases.length == 0) return '';
    return phrases[phrases.length-1];
  }

  updateSuggestion (phraseToSearch){
    var self = this;
    if (phraseToSearch === ''){
      return;
    }
    if(phraseToSearch === '*'){
      if (self.queryBoxStore.selectedOption === QueryBoxConstant.Architecture){
        self.queryBoxStore.suggestions = {
          detail: 'Any domain',
          displayText: '*',
        };
        return;
      }
    }

    if (self.queryBoxStore.selectedOption === QueryBoxConstant.Architecture 
        || self.queryBoxStore.selectedOption === QueryBoxConstant.Domain ){
      this.queryService.autocompleteDomain(phraseToSearch)
        .then(function(data){
          /* sample data
            [
              {
                "description": "Forkhead domain", 
                "pfamA_acc": "PF00250", 
                "pfamA_id": "Forkhead"
              }, 
              {
                "description": "Forkhead N-terminal region", 
                "pfamA_acc": "PF08430", 
                "pfamA_id": "Forkhead_N"
              }
            ]
          */
          // note that we don't use pfamA_id
          if (!data || data.length === 0){
            self.queryBoxStore.suggestions = [QueryBoxConstant.NO_MATCH_SUGGESTION];
            return;
          }
          var suggestions = data.map((d)=>{
            return {
              detail: d['description'],
              displayText: d['pfamA_acc'],
            };
          });
          self.queryBoxStore.suggestions = suggestions;
        });
      return;
    }
    if (self.queryBoxStore.selectedOption === QueryBoxConstant.Taxonomy){
      this.queryService.autocompleteTaxonomy(phraseToSearch)
        .then(function(data){
          /* sample data
            [
              {
                "taxId": 9060,
                "species": "Homo Sapien"
              }
            ]
          */
          if (!data || data.length === 0){
            self.queryBoxStore.suggestions = [QueryBoxConstant.NO_MATCH_SUGGESTION];
            return;
          }
          var suggestions = data.map((d)=>{
            return {
              detail: d['species'],
              displayText: d['taxId'],
            };
          });
          self.queryBoxStore.suggestions = suggestions;
        });
      return;
    }
    // for testing purpose
    function dummyUpdate(){
      setTimeout(function(){
        var entry = {
          detail: 'hello there ' + new Date(),
          displayText: 'PF00123'
        };
        self.queryBoxStore.suggestions = [];
        var r = _.random(10);
        for (var i=0;i<r;i++){
          self.queryBoxStore.suggestions.push(entry);
        }
      },300);
    }
    
  }
  updateSuggestionDebounced = _.debounce(this.updateSuggestion, 200);

  handleQueryTextChanged(queryText){
    var queryBoxStore = this.queryBoxStore;
    queryBoxStore.query = queryText; // maintain state consistency
    queryBoxStore.warning = null;
    queryBoxStore.suggestions = []; // clear all suggestions
    try{
      var autocompletePhrase = this.getAutocompletePhrase(queryText);
      this.updateSuggestionDebounced(autocompletePhrase);
    }catch (err){
      queryBoxStore.warning = {message: err.message};
      return;
    }
  }
  handleQuerySuggestionClicked(suggestion){
    this.queryBoxStore.suggestions = [];
    if (_.isEqual(suggestion, QueryBoxConstant.NO_MATCH_SUGGESTION)){
      return;
    }
    var phrases = this.getQueryPhrases(this.queryBoxStore.query);
    if (phrases.length === 0){
      phrases = [suggestion.displayText]
    }else{
      phrases[phrases.length-1] = suggestion.displayText;
    }
    this.queryBoxStore.query = phrases.concat(['']).join(', ');
  }
  handleTaxonomyQuery(){
    var {queryBoxStore,treeStore,summaryBoxStore, queryService} = this;
    var {query, selectedOption} = queryBoxStore;
    // treeStore.resetToDefaultDisplayable(); don't reset view
    if (!/( *\d+ *, *)*/.test(query)){
      queryBoxStore.warning = {
        message: 'When selecting for taxonomy option, enter comma seperated ncbi taxonomy id. The query is invalid.'
      };
      return;
    }
    var taxIds = query.split(',')
      .map((x)=>x.replace(/ /g,''))
      .filter((x)=>x.length>0)
      .map((x)=>parseInt(x));
    treeStore.startLoading();
    var hits = treeStore.getHighlightedNodesByTaxId(taxIds);
    if (!hits || hits.length === 0){
      queryBoxStore.warning = {
        message: 'Query returned no result.'
      };
    }else{
      treeStore.highlightTaxNodes(hits);
    }
    treeStore.stopLoading();
    // queryService.queryTaxonomy(taxIds)
    //   .then(function(hits){
    //     treeStore.highlightTaxNodes(hits);
    //   })
    //   .catch(function(err){
    //     console.log(err);
    //     queryBoxStore.warning = {
    //       message: err.error,
    //     };
    //   }).then(function(){
    //     treeStore.stopLoading();
    //   });
    return;
  }
  handleDomainAndArchitectureQuery(){
    var {queryBoxStore,treeStore,summaryBoxStore, queryService} = this;
    var {query, selectedOption} = queryBoxStore;
    var phrases = this.getQueryPhrases(query);
    if (!phrases || phrases.length === 0){
      queryBoxStore.warning = {
        message: 'You have entered invalid or empty query.'
      };
      return;
    }

    treeStore.startLoading();
    // treeStore.resetToDefaultDisplayable(); dont' reset view
    summaryBoxStore.clear();
    submitRequest(phrases, queryBoxStore.selectedOption)
      .then(function(data){
        var hits = data['hits'];
        treeStore.highlightNodes(hits);
        var distribution = treeStore.getDistribution(hits);
        var queryUsed = data['queryUsed'];
        summaryBoxStore.setSummary({distribution,queryUsed});
      })
      .catch(function(err){
        console.log(err);
        queryBoxStore.warning = {
          message: err.error,
        };
      }).then(function(){
        treeStore.stopLoading();
      });
    function submitRequest(queryPhrases, option){
      if (option === QueryBoxConstant.Domain){
        return queryService.queryDomains({
          domains: queryPhrases,
        });
      }else if(option === QueryBoxConstant.Architecture){
        return queryService.queryArchitecture({
          architecture: queryPhrases
        });
      }
      throw 'Unsupported option';
    }
  }
  handleQuerySubmitted(){
    var {queryBoxStore,treeStore,summaryBoxStore, queryService} = this;
    var {query, selectedOption} = queryBoxStore;
    // update URL
    var url = window.location.href.split('#/?')[0];
    var queryParam = 'qtype=' + queryBoxStore.selectedOption.displayText + '&qstring=' + 
      encodeURIComponent(query);
    window.location.href = url + '#/?' + queryParam;

    if (selectedOption === QueryBoxConstant.Taxonomy){
      this.handleTaxonomyQuery();
      return;
    }
    this.handleDomainAndArchitectureQuery();
  }
  handleFileUploaded(files){
    var {queryBoxStore, treeStore, queryService} = this;
    if (files.length === 0){
      return;
    }
    if (files.length > 1){
      alert('please upload one file');
      return;
    }
    var f = files[0];
    var reader = new FileReader();
    reader.readAsText(f);
    var parser = new window.DOMParser();

    reader.onload = function(e){
      var speciesIdList;
      try{
        var xmlDoc = parser.parseFromString(reader.result,"text/xml");
        var taxids = xmlDoc.getElementsByTagName('taxid');
        if (taxids.length === 0){
          alert('The xml does not have any hit. Taxonomy ids cannot be found.')
        }
        var speciesIdSet = {}; // use a hash to deduplicate
        for(var i = 0;i<taxids.length;i++){
          speciesIdSet[taxids[i].innerHTML] = true;
        }
        speciesIdList = Object.keys(speciesIdSet);
        console.log('species id list: ', speciesIdList);
        treeStore.startLoading();
        queryService
          .getNodeIdByTaxIds(speciesIdList)
          .then(function(nodeIds){
            var idObjects = nodeIds.map(function(id){
              return {'id':id};
            });
            treeStore.highlightTaxNodes(idObjects);
          })
          .catch(function(err){
            console.log(err);
            queryBoxStore.warning = 'Server error while processing BLAST xml,'+
              ' please take a screenshot of website console and forward it to developers';
          })
          .then(function(){
            treeStore.stopLoading();
          });
      }catch(e){
        alert('An error occured reading BLAST xml, is the format correct?');
        console.log(e);
        return;
      }
    }
   }
  handleAction(action){
    var queryBoxStore = this.queryBoxStore;
    switch(action.type){
      case QueryBoxConstant.QUERY_OPTION_CHANGED:
        var option = action.payload;
        queryBoxStore.selectedOption = option;
        queryBoxStore.optionsShown = false;
        break;
      case QueryBoxConstant.QUERY_OPTION_OPENED:
        queryBoxStore.optionsShown = !queryBoxStore.optionsShown;
        break;
      case QueryBoxConstant.QUERY_TEXT_CHANGED:
        this.handleQueryTextChanged(action.payload);
        break;
      case QueryBoxConstant.QUERY_SUBMITTED:
        this.handleQuerySubmitted();
        break;
      case QueryBoxConstant.QUERY_SUGGESTION_CLICKED:
        this.handleQuerySuggestionClicked(action.payload);
        break;
      case QueryBoxConstant.FILE_UPLOADED:
        this.handleFileUploaded(action.payload);
        break;
      case AppConstant.APP_CLICKED:
        queryBoxStore.optionsShown = false;
        queryBoxStore.suggestions = []; // clear the shown suggestions
        break;
      default:
        break;
    }
    return;
  }
};







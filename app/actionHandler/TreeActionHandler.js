import ViewActionHandler from './ViewActionHandler';
import * as TreeConstant from 'treeOfLife/TreeConstant';
import {exportToCsv,toNewick,downloadAsFile} from 'util/';
export default class TreeActionHandler extends ViewActionHandler{
  constructor(props){
    super(props);
    this.treeStore = this.stores.treeStore;
    this.treeService = this.services.treeService; // get tree service class 
    this.summaryBoxStore = this.stores.summaryBoxStore;
  }
  downloadSVG(svg){
    // index html has this already
    function setInlineStyles(svg, emptySvgDeclarationComputed) {

      function explicitlySetStyle (element) {
        var cSSStyleDeclarationComputed = getComputedStyle(element);
        var i, len, key, value;
        var computedStyleStr = "";
        for (i=0, len=cSSStyleDeclarationComputed.length; i<len; i++) {
          key=cSSStyleDeclarationComputed[i];
          value=cSSStyleDeclarationComputed.getPropertyValue(key);
          if (value!==emptySvgDeclarationComputed.getPropertyValue(key)) {
            computedStyleStr+=key+":"+value+";";
          }
        }
        element.setAttribute('style', computedStyleStr);
      }
      function traverse(obj){
        var tree = [];
        tree.push(obj);
        visit(obj);
        function visit(node) {
          if (node && node.hasChildNodes()) {
            var child = node.firstChild;
            while (child) {
              if (child.nodeType === 1 && child.nodeName != 'SCRIPT'){
                tree.push(child);
                visit(child);
              }
              child = child.nextSibling;
            }
          }
        }
        return tree;
      }
      // hardcode computed css styles inside svg
      var allElements = traverse(svg);
      var i = allElements.length;
      while (i--){
        explicitlySetStyle(allElements[i]);
      }
    }
    var emptySvgDeclarationComputed = window.getComputedStyle(document.getElementById('emptysvg'));
    setInlineStyles(svg,emptySvgDeclarationComputed);
    
    var serializer = new XMLSerializer();
    var svgData = serializer.serializeToString(svg);
    var svgBlob = new Blob([svgData], {type:"image/svg+xml;charset=utf-8"});
    var svgUrl = URL.createObjectURL(svgBlob);
    var downloadLink = document.createElement("a");
    downloadLink.href = svgUrl;
    downloadLink.download = "tree.svg";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  }
  
  handleAction(action){
    var treeStore = this.treeStore;
    var summaryBoxStore = this.summaryBoxStore;
    var treeService = this.treeService;
    switch(action.type){
      case TreeConstant.TREE_BACK_BUTTON_CLICKED:
        treeStore.setLastDisplayable();
        break;
      case TreeConstant.TREE_RESET_BUTTON_CLICKED:
        treeStore.resetToDefaultDisplayable();
        break;
      case TreeConstant.TREE_DOWNLOAD_SVG_CLICKED:
        var svg = action.payload;
        this.downloadSVG(svg);
        break;

      case TreeConstant.TREE_DOWNLOAD_NEWICK_CLICKED:
        var newickFile = toNewick(treeStore.displayRoot);
        downloadAsFile(newickFile,'tree_of_life.newick');
        break;

      case TreeConstant.TREE_TYPE_CHANGED:
        var newTreeType = action.payload;
        treeStore.setTreeType(newTreeType);
        treeStore.clearHistory();
        break;

      case TreeConstant.TREE_LEVEL_CLICKED:
        var level = action.payload;
        var levelIndex = TreeConstant.AVAILABLE_LEVELS.indexOf(level);
        var currIndex = TreeConstant.AVAILABLE_LEVELS.indexOf(treeStore.currentDisplayLevel);
        treeStore.forceDisplayLevel(level);
        break;

      case TreeConstant.TREE_LABEL_CLICKED:
      case TreeConstant.TREE_INTERNAL_NODE_CLICKED: 
        var {node,containerCoord} = action.payload;
        treeStore.setNodeDetail(node,containerCoord);
        break;

      case TreeConstant.TREE_POP_UP_CLOSE: 
        treeStore.nodeDetail = null;
        break;

      case TreeConstant.TREE_RECENTER_CLICKED:
        var node = action.payload;
        treeStore.nodeDetail = null;
        treeStore.recenterAt(node);
        break;

      case TreeConstant.TREE_MANUAL_LABEL_SUBMITTED:
        var labelDetail = action.payload;
        if (labelDetail.manualRank !== 'phylum' && labelDetail.manualRank !== 'class'){
          alert('Only phylum and class are allowed!');
          return;
        }
        treeService.manualLabel(labelDetail)
          .catch(function(err){
            console.log(err);
            alert('something went wrong submitting manual label, check console log');
          });
        break;

      case TreeConstant.TREE_DOWNLOAD_PROTEIN_SEQS:
        var nodeDetail = treeStore.nodeDetail;
        var rows = nodeDetail.formatProteinHitsToRows();
        var metaData = [['query used: ', summaryBoxStore.queryUsed],
          ['node taxon name: ', nodeDetail.taxonName],
          ['node id: ', nodeDetail.node.id]];
        exportToCsv('protein_hits.csv',metaData.concat(rows));
        break;
      case TreeConstant.TREE_FONT_SIZE_CHANGED:
        var size = action.payload;
        if (size < 1){
          return; // cannot be smaller than 1
        }
        treeStore.setFontSize(size);
        break;
      case TreeConstant.TREE_MAIN_GROUP_LABEL_CHANGED:
        treeStore.mainGroupLabelShown = !treeStore.mainGroupLabelShown;
        treeStore.setCurrentDisplayable();
        break;
      default:
        break;
    }
    return;
  }
};






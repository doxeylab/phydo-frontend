import React, {PropTypes,Component} from 'react';
import DonutChartDrawer from './DonutChartDrawer';
import {findDOMNode} from 'react-dom';

import './DonutChart.less';
export default class DonutChart extends Component{
  constructor(props) {
    super(props);
  }
  static proptypes = {
    width: React.PropTypes.number.isRequired,
    height: React.PropTypes.number.isRequired,
    data: React.PropTypes.array.isRequired,
  };
  render(){
    return (
      <div className="donutChart"></div>
      );
  }

  componentDidMount(){
    var domNode = findDOMNode(this);
    var {width,height,data} = this.props; 
    this.donutChartDrawer = new DonutChartDrawer({domNode,width,height});
    this.donutChartDrawer.draw(data);
  }
  componentDidUpdate() {
    if(this.donutChartDrawer) this.donutChartDrawer.draw(this.props.data);
  }
};




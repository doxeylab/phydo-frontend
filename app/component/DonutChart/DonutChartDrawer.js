import * as _ from 'lodash';
export default class DonutChartDrawer{
  constructor(props){
    var {domNode,width,height} = props;
    if (!width || !domNode || !height){
      throw 'UNDEFINED PROPERTIES';
    }
    var pieChartProportion = 0.5;
    var legendProportion = 1 - pieChartProportion;
    var radius = Math.min(width*pieChartProportion, height) / 2;
    // assign those properties to `this`
    this.domNode = domNode;
    this.width = width;
    this.height = height;
    this.radius = radius; 

    this.pieChartCenter = [width * pieChartProportion/2, height/2];
    this.lengendContainerCenter = [width * (pieChartProportion + legendProportion * 0.3), height/2];
    this.svg = d3.select(domNode)
      .append('svg')
      .attr('width',width)
      .attr('height',height)
      .append('g');

    this.svg.append("g")
      .attr("class", "slices")
      .attr('transform','translate(' + this.pieChartCenter[0] + ',' + this.pieChartCenter[1] + ')');
    this.svg.append("g")
    .attr("class", "centerText")
    .attr('transform','translate(' + this.pieChartCenter[0] + ',' + this.pieChartCenter[1] + ')');

    this.svg.append('g')
      .attr('class','legendContainer')
      .attr('transform','translate(' + this.lengendContainerCenter[0] + ',' + this.lengendContainerCenter[1] + ')');
    // this.svg.append("g")
      // .attr("class", "labelName");
    // this.svg.append("g")
      // .attr("class", "labelValue");
    // this.svg.append("g")
      // .attr("class", "lines");
    this.pie = d3.layout.pie()
      .sort(null)
      .value(function(d) {
        return d.value;
      });

    this.arc = d3.svg.arc()
      .outerRadius(radius * 0.8)
      .innerRadius(radius * 0.4);

    this.outerArc = d3.svg.arc()
      .innerRadius(radius * 0.9)
      .outerRadius(radius * 0.9);

    this.legendRectSize = Math.max((radius * 0.05), 14);
    this.legendSpacing = Math.max(radius * 0.02, 4);


    this.colorRange = d3.scale.category20();
    this.color = d3.scale.ordinal()
      .range(this.colorRange.range());

  }
  draw(data) {
    var {svg,pie,arc,outerArc,
      colorRange,color,legendRectSize, 
      legendSpacing, radius} = this;
    /* ------- PIE SLICES -------*/
    var totalVal = _.sumBy(data,'value');

    var slice = svg
        .select(".slices")
        .selectAll("path.slice")
        .data(pie(data), function(d){ return d.data.label });

      slice.enter()
          .insert("path")
          .style("fill", function(d) { return color(d.data.label); })
          .attr("class", "slice");

      slice
          .transition().duration(1000)
          .attrTween("d", function(d) {
              this._current = this._current || d;
              var interpolate = d3.interpolate(this._current, d);
              this._current = interpolate(0);
              return function(t) {
                  return arc(interpolate(t));
              };
          })

      slice.exit()
          .remove();
      var centerText = svg
        .select('.centerText');
      var centerTextMain = centerText
        .append('text')
        .attr('text-anchor','middle')
        .attr('class','centerText-main')
        .text(totalVal);

      centerText
        .append('text')
        .attr('text-anchor','middle')
        .attr('class','centerText-sub')
        .text('protein hits')
        .attr('transform','translate(0,14)');

      var legend = svg
          .select('.legendContainer')
          .selectAll('.legend')
          .data(color.domain())
          .enter()
          .append('g')
          .attr('class', 'legend')
          .attr('transform', function(d, i) {
              var height = legendRectSize + legendSpacing;
              var offset =  height * color.domain().length / 2;
              var horz = -3 * legendRectSize;
              var vert = i * height - offset;
              return 'translate(' + horz + ',' + vert + ')';
          });

      legend.append('rect')
          .attr('width', legendRectSize)
          .attr('height', legendRectSize)
          .style('fill', color)
          .style('stroke', color);

      legend.append('text')
          .attr('x', legendRectSize + legendSpacing)
          .attr('y', legendRectSize - legendSpacing)
          .text(function(d) { return d; });
      slice
          .on("mouseover", function(d){
              d3.select(this).classed('highlighted', true);
              legend.filter(function(l){
                return l === d.data.label;
              }).classed('highlighted',true);
              centerTextMain.text(d.data.value);
          });
      slice
          .on("mouseout", function(d){
              d3.select(this).classed('highlighted', false);
              legend.filter(function(l){
                return l === d.data.label;
              }).classed('highlighted',false);
              centerTextMain.text(totalVal);
          });
      /* ------- TEXT LABELS -------*/

      // var text = svg.select(".labelName").selectAll("text")
      //     .data(pie(data), function(d){ return d.data.label });

      // text.enter()
      //     .append("text")
      //     .attr("dy", ".35em")
      //     .text(function(d) {
      //         return (d.data.label+": "+d.value+"%");
      //     });

      // function midAngle(d){
      //     return d.startAngle + (d.endAngle - d.startAngle)/2;
      // }

      // text
      //     .transition().duration(1000)
      //     .attrTween("transform", function(d) {
      //         this._current = this._current || d;
      //         var interpolate = d3.interpolate(this._current, d);
      //         this._current = interpolate(0);
      //         return function(t) {
      //             var d2 = interpolate(t);
      //             var pos = outerArc.centroid(d2);
      //             pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
      //             return "translate("+ pos +")";
      //         };
      //     })
      //     .styleTween("text-anchor", function(d){
      //         this._current = this._current || d;
      //         var interpolate = d3.interpolate(this._current, d);
      //         this._current = interpolate(0);
      //         return function(t) {
      //             var d2 = interpolate(t);
      //             return midAngle(d2) < Math.PI ? "start":"end";
      //         };
      //     })
      //     .text(function(d) {
      //         return (d.data.label+": "+d.value+"%");
      //     });


      // text.exit()
      //     .remove();

      /* ------- SLICE TO TEXT POLYLINES -------*/

      // var polyline = svg.select(".lines").selectAll("polyline")
      //     .data(pie(data), function(d){ return d.data.label });

      // polyline.enter()
      //     .append("polyline");

      // polyline.transition().duration(1000)
      //     .attrTween("points", function(d){
      //         this._current = this._current || d;
      //         var interpolate = d3.interpolate(this._current, d);
      //         this._current = interpolate(0);
      //         return function(t) {
      //             var d2 = interpolate(t);
      //             var pos = outerArc.centroid(d2);
      //             pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
      //             return [arc.centroid(d2), outerArc.centroid(d2), pos];
      //         };
      //     });

      // polyline.exit()
      //     .remove();
  };
};
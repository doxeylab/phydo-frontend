export const QUERY_TEXT_CHANGED = 'QUERY_TEXT_CHANGED';
export const QUERY_OPTION_CHANGED = 'QUERY_OPTION_CHANGED';
export const QUERY_OPTION_OPENED = 'QUERY_OPTION_OPENED';
export const QUERY_SUGGESTION_CLICKED = 'QUERY_SUGGESTION_CLICKED';
export const QUERY_SUBMITTED = 'QUERY_TEXT_SUBMITTED';
export const FILE_UPLOADED = 'FILE_UPLOADED';


const Domain = {
  displayText: 'Domain',
  placeholder: 'Search any protein containing these domains',
};

const Architecture = {
  displayText: 'Architecture',
  placeholder: 'Search domains in specified order, use * for wildcard'
};

const Taxonomy = {
  displayText: 'Taxonomy',
  placeholder: 'Enter a comma separated list of NCBI GENUS ids'
};

export {Domain,Architecture,Taxonomy};
export const QUERY_BOX_OPTIONS = [Domain, Architecture, Taxonomy];
export const NO_MATCH_SUGGESTION = {
  detail: 'Sorry nothing matched your query',
  displayText: '',
};


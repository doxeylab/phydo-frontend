import { observable, computed, action } from 'mobx';
import {QUERY_BOX_OPTIONS,Domain} from './QueryBoxConstant';

export default class QueryBoxStore{
  @observable options = QUERY_BOX_OPTIONS;
  @observable selectedOption = Domain;
  @observable optionsShown = false;
  @observable suggestions = [];
  @observable query = '';
  @observable warning = null;
  constructor() {
    // don't do any thing
  }
}




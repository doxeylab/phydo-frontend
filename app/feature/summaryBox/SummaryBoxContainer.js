import React, {PropTypes,Component} from 'react';
import {observer} from "mobx-react";
// import QueryBoxView from './QueryBoxView';
import * as SummaryBoxConstant from './SummaryBoxConstant';
import AppDispatcher from 'dispatcher/AppDispatcher';
import DonutChart from 'DonutChart/';
import SimpleBox from 'SimpleBox/'
import './SummaryBox.less';

const width = 400, height = 320;

@observer
export default class SummaryBoxContainer extends Component{
  static proptypes = {
    summaryBoxStore: React.PropTypes.object.isRequired,
  };
  render(){
    var {summaryBoxStore} = this.props;
    var {distribution, queryUsed, hasSummary} = summaryBoxStore;
    return (
      <div className="summaryBox">
        <SimpleBox titleLeft="Summary">
          {hasSummary?(<div>
            <p className="form-label">Query used:</p>
            <p>{queryUsed}</p>
            <DonutChart width={width} height={height} data={distribution}></DonutChart>
          </div>): <p className="form-label">Oops, no query result yet</p>}
        </SimpleBox>
      </div>
      )
  }
}





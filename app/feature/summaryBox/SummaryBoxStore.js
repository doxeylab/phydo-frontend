import { observable, computed, action } from 'mobx';

export default class SummaryBoxStore{
  @observable distribution = [];
  /* [{
    label: <string>,
    value: <number>, 
  }]
  */
  @observable queryUsed = ''; //String
  @observable hasSummary = false; //bool

  @action clear(){
    this.distribution = [];
    this.queryUsed = '';
    this.hasSummary = false;
  }
  @action setSummary(props){
    this.distribution = props.distribution;
    this.queryUsed = props.queryUsed;
    this.hasSummary = true;
  }
}



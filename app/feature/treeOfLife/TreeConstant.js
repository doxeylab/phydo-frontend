export const TREE_LABEL_CLICKED = 'TREE_LABEL_CLICKED';
export const TREE_BACK_BUTTON_CLICKED = 'TREE_BACK_BUTTON_CLICKED';
export const TREE_RESET_BUTTON_CLICKED = 'TREE_RESET_BUTTON_CLICKED';
export const TREE_DOWNLOAD_SVG_CLICKED = 'TREE_DOWNLOAD_SVG_CLICKED';
export const TREE_DOWNLOAD_NEWICK_CLICKED = 'TREE_DOWNLOAD_NEWICK_CLICKED';
export const TREE_INTERNAL_NODE_CLICKED = 'TREE_INTERNAL_NODE_CLICKED';
export const TREE_POP_UP_CLOSE = 'TREE_POP_UP_CLOSE';
export const TREE_RECENTER_CLICKED = 'TREE_RECENTER_CLICKED';
export const TREE_MANUAL_LABEL_SUBMITTED = 'TREE_MANUAL_LABEL_SUBMITTED';
export const TREE_TYPE_CHANGED = 'TREE_TYPE_CHANGED';
export const TREE_LEVEL_CLICKED = 'TREE_LEVEL_CLICKED';
export const TREE_DOWNLOAD_PROTEIN_SEQS = 'TREE_DOWNLOAD_PROTEIN_SEQS';
export const TREE_FONT_SIZE_CHANGED = 'TREE_FONT_SIZE_CHANGED';
export const TREE_MAIN_GROUP_LABEL_CHANGED = 'TREE_MAIN_GROUP_LABEL_CHANGED';


export const PRUNED_TREE = 'Pruned Tree';
export const UNPRUNED_TREE = 'Unpruned Tree';

export const INFERRED_ANCESTOR = 'Inferred Ancestor';
export const GENUS_REP = 'Genus Rep.';
export const CLASS_REP = 'Class Rep.';
export const PHYLUM_REP = 'PHYLUM Rep.';

export const GENUS = 'genus'; // highest resolution
export const CLASS = 'class'; // highest resolution
export const PHYLUM = 'phylum';
export const AVAILABLE_LEVELS = [PHYLUM, GENUS]; // CLASS level removed as of Feb 15, 2017

export const NUM_TOP_HITS_SHOWN = 5;

export const NODE_CONTAINER_WIDTH = 275;

export const PHYLUM_MANUAL_NAMING = {
	'12': 'Proteobacteria (alpha, beta, gamma, zeta)',
	'1216': 'Proteobacteria Delta',
};

// using genus name for all single lineage eukaryotes
// up to date as of Oct 16, 2017
export const PHYLUM_MANUAL_USE_GENUS_NAME = {
	// '5659': true, // "Amoebozoa"
	// '5684': true, // "Alveolata"
	// '5685': true, // "Amoebozoa"
	// '5686': true, // "Amoebozoa"
	// '5538': true, // "Arthropoda"
	// '5539': true, // "Chordata"
	// '5540': true, // "Mollusca" -> Lottia
	// '5542': true, // "Annelida" -> Helobdella
	// '5559': true, // "Nematoda" -> Caenorhabditis
	// '5562': true, // "Porifera" -> Amphimedon
	// '5563': true, // "Cnidaria" -> Nematostella
	// '10228': true, // "Placozoa" -> Trichoplax
	// '2304': true, // "Gemmatimonadetes" -> Gemmatirosa
	// '2284': true, // "Fibrobacteres" -> Chitinivibrio
	// '2271': true, // "Candidatus Cloacimonetes" -> Candidatus Cloacimonas acidaminovorans str. 
	// '2200': true, // "Deferribacteres" -> Caldithrix abyssi DSM 
	// '1665': true, // "Candidatus Omnitrophica" -> Candidatus Omnitrophus 
	// '1574': true, // "candidate division NC10" -> Candidatus Methylomirabilis oxyfera
	// '1211': true, // "Chrysiogenetes" -> Desulfurispirillum 
	// '5721': true, // "Candidatus Lokiarchaeota" -> Lokiarchaeum

};

export const DOMAIN_EUKARYOTA = 'EUKARYOTA';
export const DOMAIN_BACTERIA = 'BACTERIA';
export const DOMAIN_ARCHAEA = 'ARCHAEA';

// each node id in the following list is the ancestral node for all
// nodes belonging to the same domain
// note that there several Archaea nodes, since domain archaea is
// fragmented to many branches
// as of 2017 Oct 16, the following list is ob
export const DOMAIN_NODES = {
	'5376': DOMAIN_EUKARYOTA, // top level eukaryota
	'5721': DOMAIN_ARCHAEA, // lokiarchaeota
	'5726': DOMAIN_ARCHAEA, // TACK group, including Thaumaarchaeota, Crenarchaeota, Korarhaeota
	'5893': DOMAIN_ARCHAEA, // Euryarchaeota
	'6121': DOMAIN_ARCHAEA, // Nanoarchaeota
	'3': DOMAIN_BACTERIA, // top level bacteria, but taxon shown as root
};



import React, {PropTypes,Component} from 'react';
import {findDOMNode} from 'react-dom';
import {observer} from "mobx-react";
import TreeOfLife from './TreeOfLife';
import {NodeDetailContainer} from './nodeDetail';
import * as TreeConstant from './TreeConstant';
import AppDispatcher from 'dispatcher/AppDispatcher';

import {LoadingBar} from 'LoadingBar/';

@observer
export default class TreeContainer extends Component{
  static proptypes = {
    treeStore: React.PropTypes.object.isRequired, 
  }
  constructor(props) {
    super(props);
  }
  handleBackClick(e){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_BACK_BUTTON_CLICKED,
    });
  }
  handleResetButtonClick(e){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_RESET_BUTTON_CLICKED,
    });
  }
  handleDownloadSVGClick(e){
    e.stopPropagation();
    var domNode = findDOMNode(this);
    var svgNode = domNode.getElementsByTagName('svg')[0];
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_DOWNLOAD_SVG_CLICKED,
      payload: svgNode,
    });
  }
  handleDownloadNewickClick(e){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_DOWNLOAD_NEWICK_CLICKED,
    });
  }
  handleTreeTypeChanged(e){
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_TYPE_CHANGED,
      payload: e.target.value,
    });
  }

  handleLevelClicked(e,l){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_LEVEL_CLICKED,
      payload: l,
    });
  }

  handleFontSizeChange(e,size){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_FONT_SIZE_CHANGED,
      payload: size,
    });
  }

  handleMainGroupLabelChange(e){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_MAIN_GROUP_LABEL_CHANGED
    });
  }
  getLevelClass(level, currentDisplayLevel){
    var lIndex = TreeConstant.AVAILABLE_LEVELS.indexOf(level);
    var clIndex = TreeConstant.AVAILABLE_LEVELS.indexOf(currentDisplayLevel);
    if (lIndex < clIndex){
      // return 'disabled';
      return 'enabled'; // we can also go up a level
    }else if (lIndex === clIndex){
      return 'active';
    }
    return 'enabled';
  }
  getLevels(currentDisplayLevel){
    return TreeConstant.AVAILABLE_LEVELS.map((l,i)=>
      (<li key={i} className={this.getLevelClass(l,currentDisplayLevel)}
        onClick={(e)=>this.handleLevelClicked(e,l)}>
        {l}
      </li>));
  }
  render(){
    var {treeStore} = this.props;
    var {innerRadius,outerRadius,viewDepth} = treeStore;
    return (
      <div className='treeContainer'>
        <span className='back-button' onClick={(e)=>this.handleBackClick(e)}>
          <i className="fa fa-chevron-left"></i> Back
        </span>
        <br/>
        <div className="download-button-container">
          <span className='download-button svg' onClick={(e)=>this.handleDownloadSVGClick(e)}>
            Download SVG &nbsp;<i className="fa fa-download"></i>
          </span>
          <span className='download-button newick' onClick={(e)=>this.handleDownloadNewickClick(e)}>
            Download Newick &nbsp;<i className="fa fa-download"></i>
          </span>
        </div>

        <div className="currentLevelIndicatorContainer">
          <div className="currentLevelIndicator">
            Resolution level: 
              <ul className="levelList">
                {this.getLevels(treeStore.currentDisplayLevel)}
              </ul>
          </div>
          <button className="reset-button" onClick={(e)=>this.handleResetButtonClick(e)}>
            reset view
          </button>
        </div>
        <div>
          Viewing: {treeStore.displayRoot.id === treeStore.masterTree.id?'Default tree':'Subset of '+treeStore.displayRoot.level}
        </div>
        <div>
          Tree Type: 
          <select value={treeStore.treeType} className='treeTypeToggle' onChange={(e)=>this.handleTreeTypeChanged(e)}>
            {_.keys(treeStore.trees).map((treeType)=>{
              return <option value={treeType} key={treeType}>{treeType}</option>
            })}
          </select>
        </div>
        <div>
        Font Size:
        <span className="font-minus" onClick={(event)=>this.handleFontSizeChange(event,treeStore.fontSize-1)}><i className="fa fa-minus-circle"></i></span>
        {treeStore.fontSize}
        <span className="font-plus" onClick={(event)=>this.handleFontSizeChange(event,treeStore.fontSize+1)}><i className="fa fa-plus-circle"></i></span>
        </div>
        <div>
          Show main group labels
          <input type="checkbox" onClick={(event)=>this.handleMainGroupLabelChange(event)} checked={treeStore.mainGroupLabelShown}/>
        </div>
        <TreeOfLife treeStore={treeStore}>
          {!!treeStore.nodeDetail && <NodeDetailContainer nodeDetail={treeStore.nodeDetail}></NodeDetailContainer>}
        </TreeOfLife>
        <LoadingBar isLoading={treeStore.isLoading}></LoadingBar>
        <div className="footNotes">
          *: unclassified phylum, genus name used instead
          <br/>
          ⊕: an ancestor, not genus representative
          <br/>
          greyed text: node without proteome information, in unpruned tree only
          <br/>
        </div>
      </div>
      );
  }
};




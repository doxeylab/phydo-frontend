import React, {PropTypes,Component} from 'react';
import {observer} from "mobx-react";
import * as TreeConstant from './TreeConstant';
import AppDispatcher from 'dispatcher/AppDispatcher';
import SimpleBox from 'SimpleBox/'

@observer
export default class TreePopUpBox extends Component{
  static proptypes = {
    treeStore: React.PropTypes.object.isRequired,
  }
  constructor(props) {
    super(props);
  }
  handlePopUpClose(e){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_POP_UP_CLOSE,
      payload: null,
    });
  }

  render(){
    var {nodeDetail} = this.props.treeStore;
    // only show if nodeDetail is not null
    return (
      !!nodeDetail && <div className="treePopUpBox">
        <SimpleBox 
          titleLeft="Hello" 
          titleRight={
            <span>World <i className="fa fa-close" onClick={this.handlePopUpClose}></i></span>
          }>

          <div>TODO change me!</div>
        </SimpleBox>
      </div>)
  }
}



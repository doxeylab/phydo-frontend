import * as _ from 'lodash';
const DEFAULT_HIGHLIGHT = 'red';
import { observable, computed, action } from 'mobx';
import * as TreeConstant from './TreeConstant';
import {NodeDetailModel} from './nodeDetail/';

/* 
  A class holding the representation of tree, 
  this is stateful
  */

export default class TreeStore{
  innerRadius = 0;
  outerRadius = 0;
  viewDepth = 1;
  @observable highlightedNodes = null;
  @observable highlightedTaxNodes = null; // stores highlighted nodes from taxonomy search
  @observable displayHistory = [];
  @observable displayRoot = null;
  @observable displayable = null; // object with 2 fields, `nodes` and `links`, computed from displayRoot
  @observable isLoading = false;
  @observable nodeDetail = null; // class NodeDetailModel
  @observable treeType = TreeConstant.PRUNED_TREE; // default is pruned
  @observable trees = {};
  @observable currentDisplayLevel = TreeConstant.PHYLUM; //default use phylum
  @observable fontSize = 12;
  @observable mainGroupLabelShown = true; // showing label by default
  constructor(options){
    // this master tree is only used for data, the tree in display is separate
    // the thinking is that we only want to mutate the tree in display and not the 
    // master tree
    this.trees = {
      [TreeConstant.PRUNED_TREE]: {
        tree: options.prunedTree,
        nodeMap: getIdNodeMap(options.prunedTree)
      },
      [TreeConstant.UNPRUNED_TREE]: {
        tree: options.unprunedTree,
        nodeMap: getIdNodeMap(options.unprunedTree),
      },
    };

    /*
      @var masterIdNodeMap
      {
        '123'<nodeId:number>: <node obj>
      }
    */
    this.viewDepth = options.viewDepth;
    this.innerRadius = options.innerRadius;
    this.outerRadius = options.outerRadius;
    var self = this;

    // initialize the pruned and unpruned tree by adding computed values to it
    function processTree(tree){
      // infer `counts` for each internal node
      // e.g.
      // A has children B(counts 12) C(counts 10), A.counts is set to 22
      traverseCollect(tree,function(node,childrenResult){
        var selfCount = node.counts? parseInt(node.counts):0;
        var total = selfCount + _.sum(childrenResult);
        node.counts = total;
        return total;
      });
      // adding taxonomy domain type to each node
      // e.g. all internal and leaf nodes under Eukaryote node will have taxDomainType = 'EUKARYOTES'
      // all nodes under bacteria node will have taxDomainType = 'BACTERIA'
      // top nodes above domain level will not have taxDomainType
      function addDomainType (node, taxDomainType){
        if (!node){
          return;
        }
        // if current node is the domain node, assign taxDomainType
        if (!taxDomainType){
          taxDomainType = TreeConstant.DOMAIN_NODES[''+node.id] || null;
          // turning off 5726, the TACK group
          if (taxDomainType && (node.id + '') !== '5893') node.isDomainRoot = true;
        }
        if (taxDomainType) {
          node.taxDomainType = taxDomainType;
        }
        // recursively traverse all children
        if (node.children && node.children.length > 0) {
          node.children.forEach(function(c){
            addDomainType(c, taxDomainType);
          });
        }
      }
      
      addDomainType(tree, null);

      self.cluster = d3.layout.cluster() // cluster layout compute function
        .size([360, self.innerRadius])
        // .children(function(d) { return d.branchset; })
        .children(function(d){return d.children})
        .value(function(d) { return 1; })
        .sort(function(a, b) { return (a.value - b.value) || d3.ascending(a.length, b.length); })
        .separation(function(a, b) { return 1; });
      self.cluster.links(self.cluster.nodes(tree)); // precompute depth in master tree, although it will not be used for display
    }

    // forEach tree, process it
    _.mapValues(this.trees,(treeObj)=>processTree(treeObj.tree));
    this.resetToDefaultDisplayable();
  }

  // when we access this.masterTree this method will be automatically invoked
  @computed get masterTree(){
    return this.trees[this.treeType].tree;
  }

  @computed get masterIdNodeMap(){
    return this.trees[this.treeType].nodeMap;
  }

  /*----------  Tree Display Methods  ----------*/
  // user interface level, display a new node and add to history
  // we assume that node is a deep copy of tree (i.e. not result of mutating displayRoot)
  // if want to display mutated tree, use displayCurrent()

  @action resetToDefaultDisplayable(){
    this.highlightedNodes = null;
    this.highlightedTaxNodes = null;
    this.fontSize = 12;
    this.resetToLevel(TreeConstant.PHYLUM);
    // each tree rendering will remember the previous tree state
    // a reset should clean displayHistory
    this.displayHistory = [];
  }
  @action resetToLevel(rank){
    this.currentDisplayLevel = rank;
    this.setNextLevelDisplayable();
  }
  @action setLastDisplayable(){
    if (this.displayHistory.length == 0){
      return null;
    }
    var displayHistory = this.displayHistory.pop();
    this.currentDisplayLevel = displayHistory.currentDisplayLevel;
    this.highlightedNodes = displayHistory.highlightedNodes;
    this.taxHighlightedNodes = displayHistory.taxHighlightedNodes;
    var displayRoot = displayHistory.displayRoot;
    return this._setDisplayable(displayRoot);
  }

  // display the same displayRoot as before
  // we need this function because we might change the current tree but 
  // do not necessarily want to add the change to displayHistory
  // the use case will be when collapsing the current tree
  @action setCurrentDisplayable(){
    return this._setDisplayable(this.displayRoot);
  }

  /*
  a datum is a tree node
  display the next level of tree up to a rank (e.g. phylum, class, genus)
  if datum is not given, use master root (equivalent to reset to default view)
  @param forcedRank is basically one item of TreeConstant.AVAILABLE_LEVELS
  we expect `forcedRank` to be the same, or more detailed, than rank of datum
  */
  @action setNextLevelDisplayable(datum, forcedRank){
    var self = this;
    var masterNode = datum?this.getMasterNode(datum.id):this.masterTree; // get a copy of this node in master tree
    if (!masterNode.children){
      return null; // do nothing if it is a leaf
    }
    var rank = getRank(masterNode); // get rank of datum
    // if existing rank is more detailed (reflected in index) than forced rank,
    // then return
    if (forcedRank && TreeConstant.AVAILABLE_LEVELS.indexOf(rank) > TreeConstant.AVAILABLE_LEVELS.indexOf(forcedRank)){
      return null;
    }
    // prefer forced rank
    var nextRank = forcedRank || getNextRank(rank, this.currentDisplayLevel);
    var newRoot;
    if (nextRank === TreeConstant.GENUS){
      newRoot = filterTree(masterNode, function(){return true;}); // all leaves under this node
    }else {
      newRoot = displayUpToRank(masterNode, nextRank);
      // because we might not get any 
      // intermediate level, in that case, use the highest tree resolution
      // possible
      if (!newRoot.children || newRoot.children.length === 0){
        newRoot = filterTree(masterNode, function(){return true;});
        nextRank = TreeConstant.GENUS; 
      }
    }
    this.addToDisplayHistory();
    this.currentDisplayLevel = nextRank;
    this._setDisplayable(newRoot);
    function getNextRank(nodeRank, currDisplayLevel){
      if (nodeRank !== currDisplayLevel || currDisplayLevel === TreeConstant.GENUS){
        return currDisplayLevel;
      }
      if (nodeRank === TreeConstant.PHYLUM){
        return TreeConstant.CLASS;
      }else if (nodeRank === TreeConstant.CLASS){
        return TreeConstant.GENUS;
      }else{
        throw 'Programming error: unknown rank ' + nodeRank;
      }
    }
  }
  /* A displayable object: nodes and links should be generated by d3.cluster layout function
    @return {
      nodes: [node ...],
      links: [link ...]
    }
  */
  _setDisplayable(node){
    var self = this;
    if (!node){
      throw 'Programming error: node given is null in _setDisplayable';
    }
    var masterNode = this.getMasterNode(node.id);
    if (masterNode === node){
      throw 'Programming error: you should make a deep copy of node from master tree then pass it in';
    }
    self.displayRoot = node;
    // set text
    traverseTree(self.displayRoot, function(n){
      n.text = self.textGetter(n);
      n.fontSize = self.fontSize;
      if (n.isDomainRoot) n.mainGroupLabelShown = self.mainGroupLabelShown;
    });
    setRadius(self.displayRoot, self.displayRoot.length = 0, self.innerRadius / maxLength(self.displayRoot));

    // reset highlighting here
    traverseTree(self.displayRoot, function(n){
      n.isHighlighted = false;
    });

    // highlight any internal node in the highlightNodes
    // also highlight any leaves that have children highlighted, but not shown in current display
    if (self.highlightedNodes){
      self.highlightTree(self.displayRoot,
        function highlightFn(n){
          n.isHighlighted = true;
        },
        self.isHighlightedInMaster.bind(self));
    }

    // reset highlighting here
    traverseTree(self.displayRoot, function(n){
      n.isTaxHighlighted = false;
    });

    // highlight the tree the same way as protein domains
    if(self.highlightedTaxNodes){
      self.highlightTree(self.displayRoot,
        function highlightFn(n){
          n.isTaxHighlighted = true;
        },
        function(node){
          return node && self.highlightedTaxNodes && self.highlightedTaxNodes[node.id+''];
        }
      );
    }
    var nodes = this.cluster.nodes(self.displayRoot);
    var links = this.cluster.links(nodes);
    self.displayable = {nodes,links};
  }

  /*
    given [
        {
          id: 123, // node id
          otherInfo1: blah,
          otherInfo2: blah
        }
      ]
    highlight those nodes in the array supplied
    */
  @action highlightNodes(nodeInfo){
    var map = _.keyBy(nodeInfo,function(d){
      return d.id +'';
    });
    /*
      map is an object where keys are id's
      {id:<nodeInfo>}
    */
    this.highlightedNodes = map;
    this.highlightedTaxNodes = null; // deactivate taxonomy highlighting
    this.setCurrentDisplayable();
  }

  // nodeInfo is in the same shape as nodeInfo in `highlightNodes`
  @action highlightTaxNodes(nodeInfo){
    var map = _.keyBy(nodeInfo,function(d){
      return d.id +'';
    });
    /*
      map is an object where keys are id's
      {id:<nodeInfo>}
    */
    this.highlightedTaxNodes = map;
    this.highlightedNodes = null; // deactive protein domain highlighting
    this.setCurrentDisplayable();
  }

  @action recenterAt(node){
    // when recentering, go directly to genus resolution
    this.setNextLevelDisplayable(node, TreeConstant.GENUS);
  }

  @action forceDisplayLevel(rank){
    // equivalent to recentering at current display root, but with a 
    // different rank
    this.setNextLevelDisplayable(this.displayRoot,rank);
  }
  /*----------  Other related Display Methods  ----------*/
  @action startLoading(){
    this.isLoading = true;
  }
  @action stopLoading(){
    this.isLoading = false;
  }
  /*
    Given a node and a coordinate at where the detail box should be displayed
    compute and mutate `nodeDetail` in treeStore
  */
  @action setNodeDetail(node, coordinate){
    var proteinHits = this.getProteinHits(node);
    var newNodeDetail = new NodeDetailModel({
      displayAt: coordinate,
      node: node,
      taxonName: getTaxonName(node),
      taxonId: node.taxId,
      taxonType: getTaxonType(node),
      rank: getRank(node),
      isLeaf: node.isLeaf,
      hasQueryResult: !!this.highlightedNodes,
      taxonomyHits: this.getTaxonomyHits(node),
      proteinHits: proteinHits,
    });
    this.nodeDetail = newNodeDetail;
  }

  @action setTreeType(type){
    if (type!==TreeConstant.PRUNED_TREE && type !== TreeConstant.UNPRUNED_TREE){
      throw 'Programming error: invalid type, expected PRUNED_TREE or UNPRUNED_TREE only';
    }
    this.treeType = type;
    this.resetToLevel(TreeConstant.PHYLUM);
  }

  @action setFontSize(size){
    this.fontSize = size;
    this.setCurrentDisplayable();
  }
  /*----------  Data Getters  ----------*/
  /*
    @param data: [
      {
        id: <node id>,
        seqIds: [<seq object>] // but we only care about length
      }
    ]
    @return [{
      label: <phylum name, string>,
      value: <number> the number of proteomes in that phylum
    }]
    traverse from the given node upwards until hit a phylum node
    if it hits root, consider that as "unclassified phylum"
    increment count by the length of `seqIds`
  */
  getDistribution(data) {
    var findPhylumParent = function(node){
      return selectAncestorInTree(node, (n)=>getRank(n) === TreeConstant.PHYLUM);
    }
    var self = this;
    var phylumHitCounts = data.map(function(d){
      var masterNode = self.getMasterNode(d.id);
      var phylumParent = findPhylumParent(masterNode);
      var phylumName = phylumParent?getTaxonName(phylumParent):'unclassified phylum';
      return {
        'phylum': phylumName,
        'hitCount': d.seqIds.length,
      };
    });
    var phylumDistribution = _.chain(phylumHitCounts)
      .reduce(function(acc, hitCount){
          var count = acc[hitCount['phylum']] || 0;
          count += hitCount['hitCount'];
          acc[hitCount['phylum']] = count;
          return acc;
        }, {})
      .map(function(val, key){
        return {
          label: key,
          value: val,
        };
      })
      .value();

    return phylumDistribution;
  }

  getHighlightedNodesByTaxId(taxIds) {
    // turn array to hash map
    var taxIdMap = _.reduce(taxIds,(acc,x)=>{
      acc[x] = true;
      return acc;
    },{});
    var allNodes = _.values(this.masterIdNodeMap); // all nodes in a tree
    return allNodes
      .filter(function(x){ // only retain those whose genusId is in `taxIdMap`
        return !!taxIdMap[x.genusId];
      }) 
      //leaf nodes that have the wanted genus id
      .map((x)=>{
        return {
          'id': x.id,
        }
      });
      // now an array of object [{id:<num>} ...]
  }

  // returns label text for `node`
  textGetter(node){
    if (this.currentDisplayLevel === TreeConstant.GENUS){
      var nodeName;
      if (! node.level){
        nodeName = node.name;
      }else if (node.isLeaf && !/Candidatus/.test(node.level)){ 
        // node is a leaf and does not begin with Candidatus
        // so we only use the first part of binomial naming
        // e.g. Homo sapien -> Homo
        nodeName = node.level.split(' ')[0];
      }else{
        nodeName = node.level;
      }
      return nodeName;
    }
    if (this.currentDisplayLevel === TreeConstant.PHYLUM) {
      var nodename;
      if (TreeConstant.PHYLUM_MANUAL_NAMING[''+node.id]) {
        nodename = TreeConstant.PHYLUM_MANUAL_NAMING[''+node.id];
      }else if (node.phylumTaxId === 0 || TreeConstant.PHYLUM_MANUAL_USE_GENUS_NAME[''+node.id]){
        // phylum is unclassified
        // we'd like to display genus name appended with *
        if (node.level){
          // e.g. nameParts can be either
          // ['Candidatus' 'Cloacimonas' 'acidaminovorans']
          // or ['Methylomirabilis' 'oxyfera']
          // we prefer the first non candidatus part
          var nameParts = node.level.split(' ');
          if (/Candidatus/.test(nameParts[0])){
            nodename = nameParts[1];
          }else{
            nodename = nameParts[0];
          }
          nodename = nodename + '*';
        }else{
          // use the original name given in newick file
          // this is a long string
          nodename = node.name + '*';
        }
      }else if(node.phylumTaxId === node.taxId){
        // node itself is phylum
        nodename = node.phylumLevel;
      }else{
        nodename = node.phylumLevel; // optionally add more info here
      }
      return (node.isLeaf?'':'⊕') + nodename;
    }
    if (this.currentDisplayLevel === TreeConstant.CLASS){
      if (node.classTaxId === 0){
        return 'Unclassified class';
      }else if( node.classTaxId === node.taxId ){
        // node itself is class
        return node.classLevel;
      }else{
        return node.classLevel;
      }
    }
    return 'error getting text';
  }
  /*----------  Other Methods  ----------*/
  clearHistory(){
    this.displayHistory = [];
  }

  /*----------  Private methods  ----------*/

  /*
    given a tree node, 
    return the protein hit distribution for any children of `node`
    return [{
      seqId: number,
      architecture: [<arch obj>...]
    }...];
  */
  getProteinHits(node){
    var highlightedInfo = this.collectHighlightedInfo(node);
    // if highlightedInfo does not have seqIds, meaning does not contain protein hit
    // return empty
    if (!highlightedInfo || highlightedInfo.length === 0 || !highlightedInfo[0]['seqIds']){
      return [];
    }
    // basically flat map all seqIds from all tree nodes
    return _.reduce(highlightedInfo.map((x)=>x['seqIds']),function(acc,x){
      return x.concat(acc);
    },[]);
  }
  /*
    given a tree node
    return the taxonomy hit distribution
    return {
      'noHit':number,
      'hit':number,
      'noInfo':number,
    }
  */
  getTaxonomyHits(node){
    var masterNode  = this.getMasterNode(node.id);
    var self = this;
    var hits = reduceTree(masterNode, function(acc,n){
      if(n.children) return acc;
      if(!n.counts){ // if n.counts is null or 0
        acc['noInfo']++;
      }else if(self.getHighlightedInfo(n)){
        acc['hit']++;
      }else{
        acc['noHit']++;
      }
      return acc;
    }, {
      'noHit':0,
      'hit':0,
      'noInfo':0,
    })
    if (hits['noHit'] === 0 && hits['hit'] === 0 && hits['noInfo'] === 0){
      return null; //no interesting distribution
    }
    return hits;
  }

  // checks if the current node or any of its children is highlighted, 
  // as determined by highlightSelector
  isHighlighted(n,highlightSelector){
    var self = this;
    if (highlightSelector(n)){
      return true;
    }else if (!n.children || n.children.length === 0){
      // if leaf, check if any of its children matches highlight 
      // criteria, if true, highlight that
      var masterNode = self.getMasterNode(n.id);
      var hasChildrenHighlighted = selectOneInTree(masterNode,highlightSelector);
      return hasChildrenHighlighted;
    }
  }
  // apply `highlightFn` to all leaves where `isHighlighted()` return true
  // and also the entire path from leaves to the root
  highlightTree(n, highlightFn, highlightSelector){
    var self = this;
    var isSelfOrChildHighlighted = false;
    if (self.isHighlighted(n,highlightSelector)){
      isSelfOrChildHighlighted = true;
    }
    if(n.children && n.children.length > 0){
      // if any child is highlighted, self is also highlighted
      var childHighlighted = _.chain(n.children)
        .map(function(child){
          return self.highlightTree(child,highlightFn,highlightSelector);
        })
        .reduce(function(acc,x){
          return acc || x;
        }).value();
      isSelfOrChildHighlighted = childHighlighted || isSelfOrChildHighlighted;
    }
    if (isSelfOrChildHighlighted){
      highlightFn(n); 
    }
    return isSelfOrChildHighlighted;
  }

  // tells whether a node is highlighted in the MASTER tree
  // note that this is different from tree in display
  isHighlightedInMaster(node){
    return node && this.highlightedNodes && this.highlightedNodes[node.id];
  }
  getHighlightedInfo(node){
    if (!this.highlightedNodes || !node) return null;
    return this.highlightedNodes[''+node.id];
  }

  collectHighlightedInfo(datum){
    var self = this;
    var masterNode = this.getMasterNode(datum.id);
    return reduceTree(masterNode,function(acc,n){
      if (!self.isHighlightedInMaster(n)){
        return acc;
      }
      var highlightedInfo = self.getHighlightedInfo(n);
      acc.push(highlightedInfo);
      return acc;
    },[]);
  }
  
  // get a node by id in the master tree
  // you should NOT mutate any master node
  getMasterNode(id){
    if (this.masterIdNodeMap && this.masterTree){
      return this.masterIdNodeMap[id+''];
    }
  }

  addToDisplayHistory(){
    this.displayHistory.push({
      displayRoot: this.displayRoot,
      highlightedNodes: this.highlightedNodes,
      highlightedTaxNodes: this.highlightedTaxNodes,
      currentDisplayLevel: this.currentDisplayLevel,
    });
  }
}

/*===============================================
=            Tree Manipulation Logic            =
===============================================*/
/*
  Tree algorithms and functions
*/

// Set the color of each node by recursively inheriting.
export function traverseTree(d,callback) {
  if(callback){
    var earlyStop = !!callback(d);
    if(earlyStop){
      return;
    }
  }
  if (d.children) d.children.forEach(function(c){
    traverseTree(c,callback);
  });
}

export function traverseCollect(d,collector){
  var collectResult = [];
  if(d.children){
    collectResult = d.children.map(function(c){
      return traverseCollect(c,collector);
    })
  }
  return collector(d,collectResult);
}
// traverse the entire tree from node onwards
// call reducer on each node, giving also the accumulator
// return the result of accumulator
export function reduceTree(node,reducer,initializer){
  var accumulator = initializer;
  if (typeof initializer === 'function'){
    accumulator = initializer(node);
  }
  traverseTree(node,function(n){
    accumulator = reducer(accumulator,n);
  });
  return accumulator;
}

export function getIdNodeMap(root){
  var m = {};
  function _addToMap(node){
    m[node.id+''] = node;
    if(node.children) node.children.forEach(_addToMap);
  }
  _addToMap(root);
  return m;
}

export function selectOneInTree(root,selector){
  if (selector(root) ){
    return root;
  }
  if(!root.children || root.children.length === 0){
    return null;
  }
  for(var i=0;i < root.children.length;i++){
    var result = selectOneInTree(root.children[i],selector);
    if (result != null){
      return result;
    }
  }
  return null;
}

/*
  return an array of nodes for which `selector` returns tree
*/
export function selectManyInTree(root, selector){
  var results = [];
  if (selector(root)){
    results.push(root);
  }
  if (!root.children || root.children.length === 0){
    return results;
  }
  // recursefor each child in node
  return results.concat(_.flatMap(root.children, function(child){
    return selectManyInTree(child, selector);
  }));
}

// find the first ancestor that matches selector
export function selectAncestorInTree(node, selector){
  if (!node){
    return node;
  }
  if (selector(node)){
    return node;
  }
  return selectAncestorInTree(node.parent,selector);
}

// get ancestors of node
export function getAncestors(node){
  var ancestors = [];
  while (node.parent){
    ancestors.push(node.parent);
    node = node.parent;
  }
  return ancestors;
}
export function getChildren(node){
  if (node.children && node.children.length > 0){
    return _.concat(_.toArray(node.children),_.flatMap(node.children,getChildren));
  }
  return [];
}

export function getLeaves(node){
  if (node === null) return [];
  if (!node.children || node.children.length === 0){
    return [node];
  }
  return _.flatMap(node.children,getLeaves);
}

// get all leaves of the given node list
export function getLeavesForNodes(nodes) {
  if (!nodes){
    throw 'invalid node list given';
  }
  var leaves = _.flatMap(nodes, function(n){
    return getLeaves(n);
  });
  return leaves;
}

export function resetColor(node){
  return traverseTree(node,function(d){
    d['color'] = null;
  });
}
// shallow copy
export function copyNode(node){
  var copy = {};
  _.extend(copy,node);
  return copy;
}
export function filterTree(root,filterFn){
  if (!filterFn(root)){
    return null;
  }
  function _filterTree(node,ff){
    var copy = copyNode(node);
    if(node.children){
      copy.children = node.children.filter(ff).map(function(d){
        return _filterTree(d,ff);
      });
    }
    return copy;
  }
  return _filterTree(root,filterFn);
}

// all children of d will have collapseTo pointing to d
// if any child `c` is a collapse root, set all of `c`'s children's 
// collapseTo to point to d instead
export function collapse(d){
  if (!d.children){
    return;
  }
  d.collapseRoot = true;
  d.children.forEach(function(child){
    traverseTree(child,function(c){
      c.collapseRoot = null;
      c.collapseTo = d;
    });
  });
}

export function uncollapse(d){
  if (!d.children){
    return;
  }
  d.collapseRoot = null;
  d.children.forEach(function(child){
    traverseTree(child,function(c){
      c.collapseRoot = null;
      c.collapseTo = null;
    });
  });
}

export function toggleCollapse(d){
  if (d.collapseRoot) uncollapse(d);
  else collapse(d);
}

// find all paths from root to the first nodes down the tree hierachy
// selected by `selector`
export function findPathsTo(root,selector){
  function _findPathTo(n,backtrack){
    if (selector(n)){
      // return a list with one item, which is the path
      return [backtrack.concat([n])];
    }else if(!n.children){
      return [];
    }else{
      // recursively apply _findPathTo on each child
      // collect the paths that they find
      return _.flatMap(n.children.map(function(child){
        return _findPathTo(child,backtrack.concat([n]));
      }));
    }
  }
  return _findPathTo(root,[]);
}

export function getRank(node){
  return node.rank;
}
export function getTaxonName(node){
  return node.level || node.name;
}
export function getTaxonType(node){
  var rank = getRank(node);
  if (node.isLeaf){
    if (rank === TreeConstant.PHYLUM){
      return TreeConstant.PHYLUM_REP;
    }else if (rank === TreeConstant.CLASS){
      return TreeConstant.CLASS_REP;
    }
    return TreeConstant.GENUS_REP;
  }
  return TreeConstant.INFERRED_ANCESTOR;
}
export function displayUpToRank(root, rank){
  var rankSelector;
  if (rank === TreeConstant.PHYLUM){
    rankSelector = function(node){
      // this node has either unclassified phylum (phylumTaxId = 0) or a real phylum
      return node.phylumTaxId > -1;
    }
  }else if (rank === TreeConstant.CLASS){
    rankSelector = function(node){
      return node.classTaxId > -1;
    }
  }else{
    throw 'Programming error: rank can only be one of phylum or class, given ' + rank;
  }
  // flatten the list of paths to fisrt level
  // create a set indexed by node id
  var paths = findPathsTo(root,rankSelector);
  var nodesOnPath = _.keyBy(_.flatten(paths),function(n){return n.id;}); // turn all nodes on the path to a set
  function collectNode(node){
    // assume node itself must be contained
    var copy = copyNode(node);
    if (!node.children){
      return copy;
    }
    var childrenOnPath = _.filter(copy.children,function(n){return !!nodesOnPath[n.id]});
    var childrenNotOnPath = _.filter(copy.children,function(n){return !nodesOnPath[n.id]});
    if(childrenOnPath.length == 0){
      copy.children = [];
      return copy;
    }
    // the new children is the result of collectNode for any child on path
    // and all the children not on path, excluding their grandchildren
    var newChildren = childrenOnPath
    .map(collectNode)
    .concat(childrenNotOnPath.map(function(c){
      var copy = copyNode(c);
      copy.children = [];
      return copy;
    }));
    copy.children = newChildren;
    return copy;
  }
  return collectNode(root);
}

/*=====  End of Tree Manipulation Logic  ======*/


/*==================================================
=            Graph Specific            =
==================================================*/
// Compute the maximum cumulative length of any node in the tree.
export function maxLength(d) {
  d.length = parseFloat(d.length); // convert to float first
  return d.length + (d.children && d.children.length > 0 ? d3.max(d.children, maxLength) : 0);
}

// Set the radius of each node by recursively summing and scaling the distance from the root.
export function setRadius(d, y0, k) {
  d.radius = (y0 += d.length) * k;// how far should this node be located from the center
  if (d.children) d.children.forEach(function(d) { setRadius(d, y0, k); });
}

/*=====  End of Graph Specific   ======*/




import React, {PropTypes,Component} from 'react';
import {observer} from "mobx-react";
import * as TreeConstant from '../TreeConstant';
import AppDispatcher from 'dispatcher/AppDispatcher';
import SimpleBox from 'SimpleBox/'
import './NodeDetailContainer.less';

import * as util from 'util/';
import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap.css';

import {getUniprotUrl,getPfamDomainUrl} from 'util/';

import TaxonomyHitPlot from './TaxonomyHitPlot';

import * as Config from 'Config';

@observer
export default class NodeDetailContainer extends Component{
  static proptypes = {
    nodeDetail: React.PropTypes.object.isRequired,
  }
  constructor(props) {
    super(props);
  }
  handlePopUpClose(e){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_POP_UP_CLOSE,
      payload: null,
    });
  }

  handleRecenterClicked(e,node){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_RECENTER_CLICKED,
      payload: node,
    });
  }
  getProteinHitItem(proteinHit,index){
    function getDomainItem(domain,index){
      var url = getPfamDomainUrl(domain.pfamAcc);
      return (<li key={index}><a target="_blank" href={url}>{domain.pfamId}</a></li>)
    }
    return (
        <div className="row proteinHit-item" key={index}>
          <div className="col-xs-3 seqId">
            <a href={getUniprotUrl(proteinHit.seqId)} target="_blank">{proteinHit.seqId.split('_')[0]}</a>
            <span className="badge badge-info">{proteinHit.count}</span>
          </div>
          <div className="col-xs-9 architecture">
            <ul>
              {proteinHit.architecture.map((domain,i)=>getDomainItem(domain,i))}
            </ul>
          </div>
        </div>
      )
  }

  handleManualLabelSubmitted(e){
    e.stopPropagation();
    var manualRank = this.manualRankInput.value;
    var manualTaxId = this.manualTaxonIdInput.value;
    var manualTaxonName = this.manualTaxonNameInput.value;
    var curator = this.curatorInput.value;
    var id = this.props.nodeDetail.node.id;
    this.manualRankInput.value = '';
    this.manualTaxonIdInput.value = '';
    this.manualTaxonNameInput.value = '';
    util.createCookie('curator',curator,1);
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_MANUAL_LABEL_SUBMITTED,
      payload: {
        manualRank,manualTaxId, manualTaxonName, id, curator,
      }
    });
  }

  handleDownloadProteinHits(e){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: TreeConstant.TREE_DOWNLOAD_PROTEIN_SEQS,
    });
  }

  render(){
    var {taxonName, taxonId, displayAt, taxonType, rank, isLeaf, 
      hasQueryResult, taxonomyHits, node} = this.props.nodeDetail;
    var proteinHits = this.props.nodeDetail.displayableProteinHits;
    var taxonomyPlotWidth = TreeConstant.NODE_CONTAINER_WIDTH - 30;
    var taxonomyPlotHeight = 25;
    var taxonNameSection = (
      <div key={0}>
        <div className="row">
          <div className="col-xs-4 form-label">
            Taxon: 
          </div>
          <div className="col-xs-8">
            {taxonName}
          </div>
        </div>
        <div className="row">
          <div className="col-xs-4 form-label">Taxon ID: </div>
          <div className="col-xs-8">{taxonId}</div>
        </div>
        <div className="row">
          <div className="col-xs-4 form-label">Node ID: </div>
          <div className="col-xs-8">{node.id}</div>
        </div>
      </div>);
    var rankSection = (<div key={1} className="row">
                                <div className="col-xs-4 form-label">
                                  Rank: 
                                </div>
                                <div className="col-xs-8">
                                  {rank}
                                </div>
                              </div>);
    var taxonomyHitSection = (!isLeaf && hasQueryResult && taxonomyHits && <div key={2} className="row">
                                          <div className="col-xs-12 taxonomyHits-section">
                                            <p className="form-label">Taxonomy Hits:</p>
                                            <TaxonomyHitPlot hitDistribution={taxonomyHits} 
                                             width={taxonomyPlotWidth}
                                             height={taxonomyPlotHeight}></TaxonomyHitPlot>
                                          </div>
                                        </div>);
    var recenterSection = (!isLeaf && <div key={3} className="row">
                      <div className="col-xs-12 action-section">
                        <span className="action-item" onClick={(e)=>this.handleRecenterClicked(e,node)}>
                          Zoom In <i className="fa fa-arrows-alt"></i>
                        </span>
                      </div>
                    </div>);
    var proteinHitSection = (hasQueryResult && proteinHits && proteinHits.length > 0 && 
                    <div key={4} className="row">
                      <div className="col-xs-12 proteinHits-section">
                        <p className="form-label">Protein Hits:</p>
                        {proteinHits.map((p,i)=>this.getProteinHitItem(p,i))}
                      </div>
                      <div className="col-xs-12 text-center">
                        <p className="form-label">Only top {TreeConstant.NUM_TOP_HITS_SHOWN} architectures are shown </p>
                        <a className="onHover" onClick={(e)=>this.handleDownloadProteinHits(e)}>Download All</a> 
                      </div>
                    </div>);
    var noQueryResult = (<div key={5} className="row">
        <div className="col-xs-12">
          Sorry no query was run, there is no additional information.
        </div>
      </div>);
    var manualLabelSection = (
      <div key={6} className="manualLabel-section">
        <div className="row">
          <div className="col-xs-4 form-label">
            Manual Rank: 
          </div>
          <div className="col-xs-8">
            <select ref={(input)=>{this.manualRankInput=input;}}>
              <option value="phylum">phylum</option>
              <option value="class">class</option>
            </select>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-4 form-label">
            <Tooltip
            trigger={['hover']}
            overlayStyle={{ zIndex: 1000 }}
            overlay="NCBI Taxon Id for curated phylum"
            placement="bottom"
            >
            <span>Manual Taxon Id</span>
            </Tooltip>
          </div>
          <div className="col-xs-8">
            <input type="number" ref={(input)=>{this.manualTaxonIdInput=input;}} placeholder="manual tax id.."/>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-4 form-label">
            <Tooltip
            trigger={['hover']}
            overlayStyle={{ zIndex: 1000 }}
            overlay="Your name as a curator, so that we can keep track of who entered what"
            placement="bottom"
            >
            <span>Curator</span>
            </Tooltip>
          </div>
          <div className="col-xs-8">
            <input type="text" ref={(input)=>{this.curatorInput=input;}} placeholder="Your name"/>
          </div>
        </div>
        <div className="row" style={{'display':'none'}}>
          <div className="col-xs-4 form-label">
            Manual Taxon Name: 
          </div>
          <div className="col-xs-8">
            <input type="text" ref={(input)=>{this.manualTaxonNameInput=input;}} placeholder="manual taxName.."/>
          </div>
        </div>
        <div className="row text-center">
          <button className="btn btn-primary" onClick={(e)=>this.handleManualLabelSubmitted(e)}>Submit</button>
        </div>
      </div>);

    var queryResult = hasQueryResult?
      (<div>{taxonomyHitSection} {proteinHitSection}</div>)
      :
      noQueryResult;

    var box = (<SimpleBox 
          titleLeft={taxonName} 
          titleRight={
            <span> {taxonType} <i className="fa fa-close" onClick={this.handlePopUpClose}></i></span>
          }>
            {taxonNameSection}
            {rankSection}
            <br/>
            {recenterSection}
            {queryResult}
            {
             // manualLabelSection
             // the manual label section allows user to override NCBI taxon rank for a node 
             // updated on Feb 23,2017 we reserve this feature only for internal use
            }
        </SimpleBox>);

    return (
      <div className="nodeDetailContainer" style={{
        top: displayAt[1] + 'px', // the y coord is distance from top
        left: displayAt[0] + 'px', // the x coord is dist from left
      }}>
        {box}
      </div>
      );
  }
  componentDidMount() {
    // auto save curator's name in cookie and populate it in container box
    if (this.curatorInput && this.curatorInput.value=== ''){
      var curator = util.readCookie('curator');
      if (curator) this.curatorInput.value = curator;
    }
  }
}



import { observable, computed, action } from 'mobx';
import * as _ from 'lodash';
import * as TreeConstant from '../TreeConstant';

 /*
    {
      displayAt: [x:Number,y:Number], // relative to tree of life container 
      taxonName: String,
      node: <object>, tree node object
      taxonType: enum{TreeConstant.INFERRED_ANCESTOR,TreeConstant.GENUS_REP},
      isLeaf: boolean
      rank: optional String
      hasQueryResult: boolean, true if there is currently a query result shown, false otherwise
      taxonomyHits: {
        noInfo: <number>,
        noHit: <number>,
        hit: <number>,
      },
      proteinHits: [{
        seqId: <string> uniprot sequence id,
        architecture: [{
          description: <string>,
          pfamAcc: <string>, e.g. PF00687
          pfamId: <string> e.g. Flg_N
        }]
      }]
    }
  */

export default class NodeDetailModel{
  @observable displayAt;
  @observable taxonName;
  @observable taxonId;
  @observable taxonType;
  @observable isLeaf;
  @observable rank;
  @observable hasQueryResult;
  @observable taxonomyHits;
  @observable proteinHits;
  @observable node; // the actual tree node object
  constructor(props) {
    this.displayAt = props.displayAt;
    this.taxonName = props.taxonName;
    this.taxonType = props.taxonType;
    this.taxonId = props.taxonId;
    this.isLeaf = props.isLeaf;
    this.rank = props.rank;
    this.hasQueryResult = props.hasQueryResult;
    this.taxonomyHits = props.taxonomyHits;
    this.proteinHits = props.proteinHits;
    this.node = props.node;
  }
  // return [{count:number, seqId:String, architecture:Object}]
  @computed get displayableProteinHits(){
    // group all protein by architecture and sort them 
    // by most common one first
    // return only the top five
    var archGetter = (h)=>{
      // concatenate all pfamId, separated by comma
      return h.architecture.map((a)=>a.pfamId).join(',');
    };
    var archToHit =  _.keyBy(this.proteinHits,archGetter);
    // return [[<count>, <arch key>]...]
    // counts how many proteins have the same architecture
    var countByArch = _.countBy(this.proteinHits,archGetter); // Object: <architecture string> => number
    var archCount = _.keys(countByArch).map((k)=>[countByArch[k],k]); // Array: [[<arch:string>, <count:number>]...]

    var topArchs = _.sortBy(archCount,(ac)=>ac[0])
      .reverse() // now asc order
      .splice(0,TreeConstant.NUM_TOP_HITS_SHOWN); // choose top a few
    var result = topArchs
      .map((countArchPair)=>{
        var count = countArchPair[0];
        var archKey = countArchPair[1];
        var h =  archToHit[archKey];
        return {
          'count': count,
          'seqId': h.seqId,
          'architecture': h.architecture,
        };
      });
    return result;
  }
  /* @return array of array like: 
    [["uniprot seq_id","architecture_pfam_id","architecture_pfam_acc"],
      ['AX05C','Flg_N,M27,Flg_C','PF00669,PF01235,PF00700'] ...]
  */
  formatProteinHitsToRows(){
    var header = ['uniprot seq_id','architecture_pfam_id','architecture_pfam_acc'];
    var rows = this.proteinHits.map(function(hit){
      var archPfamIds = hit.architecture.map((x)=>x.pfamId).join(',');
      var archPfamAccs = hit.architecture.map((x)=>x.pfamAcc).join(',');
      return [hit.seqId,archPfamIds,archPfamAccs];
    });
    return [header].concat(rows);
  }
};
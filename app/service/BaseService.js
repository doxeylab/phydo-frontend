

function _fetchJson(){
  return fetch.apply(null,arguments).then((response)=>{
    return response.json().then((x)=>{
      if (response.ok) return x;
      else throw x;
    });
  });
}
export default class BaseService{
  constructor(props) {
    var {baseUrl} = props;
    this.baseUrl = baseUrl;
  }
  fetchJson(endpoint,options){
    var fullEndpoint = this.baseUrl + endpoint;
    var defaultOptions = {
      mode: 'cors',
      headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
    }
    var fullOptions = Object.assign({},defaultOptions,options);
    var request = new Request(fullEndpoint, Object.assign({}, fullOptions, {
      headers: new Headers(fullOptions['headers']),
    }));
    return _fetchJson(request);
  }
  postJson(endpoint,data){
    return this.fetchJson(endpoint,{
      body: JSON.stringify(data),
      method: 'POST',
    });
  }
  getJson(endpoint){
    return this.fetchJson(endpoint,{
      method:'GET',
    });
  }
}





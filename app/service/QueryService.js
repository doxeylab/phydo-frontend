import BaseService from './BaseService';

export default class QueryService extends BaseService{
  constructor(props) {
    super(props);
  }
  /*
    expected input: 
    {
      architecture: [<string> ...]
    }
  */
  queryArchitecture(architecturePattern){
    return this.postJson('/phydo/treeNodes/by/architecture',architecturePattern);
  }
  getNodeIdByTaxIds(speciesIds){
    /*
      expect a list of string
      return a list of numbers, each number represents node id
    */
    return this.postJson('/phydo/treeNodes/by/taxIds', speciesIds);
  }
  /*
    expected input: 
    {
      domains: [<string> ...]
    }
  */
  queryDomains(domains){
    return this.postJson('/phydo/treeNodes/by/domains',domains);
  }
  
  autocompleteDomain(domainPhrase){
    return this.getJson('/phydo/pfamDomain/autocomplete?query='+domainPhrase);
  }
  autocompleteTaxonomy(taxPhrase){
    return this.getJson('/phydo/taxonomy/autocomplete?query='+taxPhrase);
  }
}







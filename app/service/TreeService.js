import BaseService from './BaseService';

export default class QueryService extends BaseService{
  constructor(props) {
    super(props);
  }
  manualLabel(labelDetail){
    /*
      expect labelDetail to be {
        id: <number>,
        manualRank: <string>,
        manualTaxId: <string>,
        manualTaxonName: optional <string>,
        curator: optional <string>,
    }
    */
    return this.postJson('/phydo/manualLabel',labelDetail);
  }
  
  getPrunedTree(){
    return this.getJson('/phydo/prunedTree');
  }

  getUnprunedTree(){
    return this.getJson('/phydo/unprunedTree');
  }
}

Known Issues
===

Squeezed Graphics
----
Try enlarge the browser window if components are squeezed
<img src="/squeezed_graphics.jpeg">

Modifications on Tree and NCBI Taxonomy
----
* Node ID 3010 (common ancestor for Firmicutes) Consists of multiple firmicutes with a few exceptions, for display convenience it is displayed as firmicutes, rather than a mixed group;
Query: `update tree_of_life.pruned_node n, pfam.taxonomy t SET n.phylum_tax_id = t.ncbi_taxid where n.id = 3010 AND t.level = 'Firmicutes';`
* Node ID 1749 is deleted, since it is a phage node that is misclassified; 
Query: `delete from pruned_node where id = 1749; update tree_of_life.pruned_node n, pfam.taxonomy t SET n.phylum_tax_id = t.ncbi_taxid where n.id = 1727 AND t.level = 'Chlamydiae';`
* Thermobaculum terrenum is a member of Chloroflexi, this is not reflected in NCBI Taxonomy and is updated manually.; 
Query: `update pfam.taxonomy set parent = 200795 WHERE level = 'Thermobaculum';UPDATE tree_of_life.pruned_node SET phylum_tax_id = 200795 WHERE id = 2828;`

My taxa don't show up in the tree
----
The pruned tree contains only taxa with a reference proteome from Pfam, your taxonomy search results may be incomplete.
